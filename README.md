<div align="center">
    <p align="center">
        <img src="./snowy-admin-web/public/img/logo.png" height="150" alt="logo"/>
    </p>
</div>

## 框架介绍

Snowy（SnowyAdmin）是国内首个国产化前后端分离快速开发平台，集成国密加解密插件，
软件层面完全符合等保测评要求，同时实现国产化机型、中间件、数据库适配，是您的不二之选！
技术框架与密码结合，让更多的人认识密码，使用密码；更是让前后分离“密”不可分。


采用SpringBoot+MybatisPlus+AntDesignVue+Vite 等更多优秀组件及前沿技术开发，注释丰富，代码简洁，开箱即用！


Snowy谐音“小诺”，恰应小诺团队名称；意思为”下雪的、纯洁的“，寓意框架追求简洁至上，大道至简。

欢迎加入QQ技术群互相解决问题：732230670

<p align="center">     
    <p align="center">
        <a href="https://gitee.com/xiaonuobase/snowy">
            <img src="https://gitee.com/xiaonuobase/snowy/badge/star.svg?theme=dark" alt="Gitee star">
        </a>
        <a href="https://gitee.com/xiaonuobase/snowy">
            <img src="https://gitee.com/xiaonuobase/snowy/badge/fork.svg?theme=dark" alt="Gitee fork">
        </a>
        <a href="https://www.antdv.com/docs/vue/introduce-cn/">
            <img src="https://img.shields.io/badge/vue-3.2-blue.svg" alt="bootstrap">
        </a> 
        <a href="http://spring.io/projects/spring-boot">
            <img src="https://img.shields.io/badge/vite-2.8-green.svg" alt="spring-boot">
        </a>
        <a href="https://www.antdv.com/docs/vue/introduce-cn/">
            <img src="https://img.shields.io/badge/vue--ant--design-3.2-blue.svg" alt="bootstrap">
        </a> 
        <a href="http://spring.io/projects/spring-boot">
            <img src="https://img.shields.io/badge/spring--boot-2.5-green.svg" alt="spring-boot">
        </a>
        <a href="http://mp.baomidou.com">
            <img src="https://img.shields.io/badge/mybatis--plus-3.5-blue.svg" alt="mybatis-plus">
        </a>  
        <a href="./LICENSE">
            <img src="https://img.shields.io/badge/license-Apache%202-red" alt="license Apache 2.0">
        </a>
    </p>
</p>

## 快速链接

下载地址：https://gitee.com/xiaonuobase/snowy

演示地址：https://snowy.xiaonuo.vip

## 支撑组件及启动

全栈工程师推荐idea

### 前端支撑
| 插件 | 版本   | 用途 |
|--- | ----- | ----- |
| node.js | 最新版 |  JavaScript运行环境 |

#### 启动前端

```
npm install
```
```
npm run dev
```
### 后端支撑
| 插件 | 版本 | 用途 |
| --- | ----- |  ----- |
| jdk | 11 / 1.8 |java环境 |
| maven | 最新版 |包管理工具 |
| redis | 最新版 | 缓存库 |
| mysql | 8.0 / 5.7 | 数据库 |

#### 启动后端
开发工具内配置好maven并在代码中配置数据库即可启动

## 代码结构


## 效果图:fire:

<table>
    <tr>
        <td><img src="https://pan.xiaonuo.vip/?explorer/share/fileOut&shareID=8XHr1wiA&path=%7BshareItemLink%3A8XHr1wiA%7D%2F"/></td>
        <td><img src="https://pan.xiaonuo.vip/?explorer/share/fileOut&shareID=8XHyk66w&path=%7BshareItemLink%3A8XHyk66w%7D%2F"/></td>
    </tr>
    <tr>
        <td><img src="https://pan.xiaonuo.vip/?explorer/share/fileOut&shareID=8XHy7cOw&path=%7BshareItemLink%3A8XHy7cOw%7D%2F"/></td>
        <td><img src="https://pan.xiaonuo.vip/?explorer/share/fileOut&shareID=8XHzPDJg&path=%7BshareItemLink%3A8XHzPDJg%7D%2F"/></td>
    </tr>
    <tr>
        <td><img src="https://pan.xiaonuo.vip/?explorer/share/fileOut&shareID=8XHzm0ng&path=%7BshareItemLink%3A8XHzm0ng%7D%2F"/></td>
        <td><img src="https://pan.xiaonuo.vip/?explorer/share/fileOut&shareID=8XHz1PZg&path=%7BshareItemLink%3A8XHz1PZg%7D%2F"/></td>
    </tr>
    <tr>
        <td><img src="https://pan.xiaonuo.vip/?explorer/share/fileOut&shareID=8XH0H8nw&path=%7BshareItemLink%3A8XH0H8nw%7D%2F"/></td>
        <td><img src="https://pan.xiaonuo.vip/?explorer/share/fileOut&shareID=8XH0c-yw&path=%7BshareItemLink%3A8XH0c-yw%7D%2F"/></td>
    </tr>
    <tr>
        <td><img src="https://pan.xiaonuo.vip/?explorer/share/fileOut&shareID=8XH0sCdg&path=%7BshareItemLink%3A8XH0sCdg%7D%2F"/></td>
        <td><img src="https://pan.xiaonuo.vip/?explorer/share/fileOut&shareID=8XH05YLw&path=%7BshareItemLink%3A8XH05YLw%7D%2F"/></td>
    </tr>
    <tr>
        <td><img src="https://pan.xiaonuo.vip/?explorer/share/fileOut&shareID=8XH1QLzQ&path=%7BshareItemLink%3A8XH1QLzQ%7D%2F"/></td>
        <td><img src="https://pan.xiaonuo.vip/?explorer/share/fileOut&shareID=8XH8Uyhg&path=%7BshareItemLink%3A8XH8Uyhg%7D%2F"/></td>
    </tr>
</table>


## 密码分步:fire:

| 功能                        | 算法类型          |
| ----------------------      | ------------- |
| 登录        | SM2前端加密，后端解密 |
| 登录登出日志        | SM2对登录登出日志做签名完整性保护存储    |
| 操作日志        | SM2对操作日志做签名完整性保护存储    |
| 用户密码        | SM3完整性保护存储，登录时做完整性校验    |
| 用户手机号        | SM4（cbc模式）加解密使用字段脱敏    |

## 官方微信群

因群达到200人以上，需加微信拉群

<table>
    <tr>
        <td>微信群</td>
        <td><img src="https://pan.xiaonuo.vip/?explorer/share/fileOut&shareID=7qwFVcdA&path=%7BshareItemLink%3A7qwFVcdA%7D%2F" width="120"/></td>
    </tr>
</table>

## 原理图解

## 团队成员

| 成员 | 技术 | 昵称 | 
| :---: | :---: | :---: | 
| 俞宝山 | 全栈 | 俞宝山 | 
| 徐玉祥 | 全栈 | 就是那个锅 | 
| 董夏雨 | 全栈 | 阿董 | 

## 曾获荣誉

<p align="center">
    <img src="https://pan.xiaonuo.vip/?explorer/share/fileOut&shareID=7xtGQLOA&path=%7BshareItemLink%3A7xtGQLOA%7D%2F"/>
</p>

## 版权说明

- Snowy生态技术框架全系版本采用 Apache License2.0协议

- 代码可用于个人项目等接私活或企业项目脚手架使用，Snowy全系开源版完全免费

- 二次开发如用于开源竞品请先联系群主沟通，未经审核视为侵权

- 请不要删除和修改Snowy源码头部的版权与作者声明及出处
